console.log('Loaded...');
// Pages
let landingPage = document.querySelector('#landing-page');
let visitorHomePage = document.querySelector('#visitor-home-page');
let visitorListeningPage = document.querySelector('#visitor-listing-page');
let visitorFiltersPage = document.querySelector('#filter-artists');
let artistHomePage = document.querySelector('#artist-home-page');
let artistHamburgerMenu = document.querySelector('#artist-menu');
let artistItemPage = document.querySelector('#artist-item-page');
let artistAddNewItemPage = document.querySelector('#artist-add-new-item-page');
let artistCaptureImagePopup = document.querySelector('#artist-capture-image-popup');
let auctionPage = document.querySelector('#auction-page');
let signUpPage = document.querySelector('#sign-up-page');

$(() => {
    console.log('jQuery...')

    const artistSelectButton = $('#artists-list');
    const typeSelectFilter = $('#filter-artists #typeArt');
    let cardsListing = $('#cards-listing');
    let cardBody = $('.card-body');
    const publishedItems = items.filter(i => i.isPublished)


    // routing:
    function handleRoute() {
        let hash = location.hash;
        if (hash == '') {
            hash = '#landing-page'
        }
        $('section').fadeOut(150).parent().find(hash).delay(150).fadeIn();
    };
    handleRoute();
    $(window).on('hashchange', handleRoute);
    // ------------------------------------------

    // adding artists in choose select tag Landing page:
    $.get('https://jsonplaceholder.typicode.com/users')
        .then(function (artists) {
            artists.forEach(function (artist) {
                $(artistSelectButton).append(`<option value"${artist.id}">${artist.name}</option>`)
            })
        })



    // listening cards in Visitor Listing Page:
    items.forEach(({ image, artist, price, title, description }) => {
        $(cardsListing).append(`
        <div class="card mt-5">
        <img class="card-img-top" src="${image}" alt="Image of artist: ${artist}"/>
        <div class="card-body">
          <h5 class="card-title h2">${artist}</h5>
          <button id="btn-price" class="btn">${price}</button>
          <p class="h6">${title}</p>
          <p class="card-text">${description}</p>
        </div>
      </div>`);
    })


    // Filters by TYPE:
    itemTypes.forEach((type) => {
        typeSelectFilter.append(`<option value="${type}">${type}</option>`)
    })

    // Choose Artist:
    $.get('https://jsonplaceholder.typicode.com/users')
        .then(function (artists) {
            artists.forEach(function (artist) {
                $('#artists-list2').append(`<option value"${artist.id}">${artist.name}</option>`)
            })
        })

    // Apply Filters Button:
    // $('#applFilters').off().on('click', function () {
    //     console.log('filtersApplied')

    //     const title = $('#title-deed').val()
    //     const artist = $('#artists-list2').val()
    //     const minPrice = $('#min-price').val()
    //     const maxPrice = $('#max-price').val()
    //     const type = $('#typeArt').val()


    //     const filteredItems = publishedItems.filter(item => {
    //         return (title ? item.title.includes(title) : true) &&
    //             (artist ? item.artist === artist : true) &&
    //             (minPrice ? item.price >= minPrice : true) &&
    //             (maxPrice ? item.price <= maxPrice : true) &&
    //             (type ? item.type === type : true)

    //     })

    //     console.log(filteredItems)

    //     $(visitorListeningPage).empty()
    //     filteredItems.forEach(({ image, artist, price, title, description }) => {

    //         visitorListeningPage
    //         //     .append(`
    //         //     <div class="card mt-5">
    //         //     <img class="card-img-top" src="${image}" alt="Image of artist: ${artist}"/>
    //         //     <div class="card-body">
    //         //       <h5 class="card-title h2">${artist}</h5>
    //         //       <button id="btn-price" class="btn">${price}</button>
    //         //       <p class="h6">${title}</p>
    //         //       <p class="card-text">${description}</p>
    //         //     </div>
    //         //   </div>`)
    //     })
    // })
    
    // adding tables:
    // function initChart() {
    //     const today = new Date()
    //     const sevenDaysAgo = new Date().setDate(new Date().getDate() - 7)
    
    //     const labels = generateDatesInBetween(sevenDaysAgo, today);
    //     console.log(labels)
    
    //     const labels2 = [
    //         '1',
    //         '2',
    //         '3',
    //         '4',
    //         '5',
    //         '6',
    //         '7',
    //         '8',
    //         '9',
    //         '10',
    //         '11',
    //         '12',
    //         '13',
    //         '14',
    //     ];
    
    
    //     const data = {
    //         labels: labels.map(d => `${d.toLocaleDateString('en-GB')}`),
    //         datasets: [{
    //             label: 'My First dataset',
    //             backgroundColor: 'rgb(255, 99, 132)',
    //             borderColor: 'rgb(255, 99, 132)',
    //             data: [0, 10, 5, 2, 20, 30, 45],
    //             backgroundColor: [
    //                 '#a26a5e'
    //             ],
    //         }]
    //     };
    
    //     const config = {
    //         type: 'bar',
    //         data,
    //         options: {
    //             indexAxis: 'y',
    //         }
    //     };
    
    //     const myChart = new Chart(
    //         document.getElementById('myChart'),
    //         config
    //     );
    
    //     $('#7days').on('click', function () {
    //         myChart.data.datasets[0].data = [0, 10, 5, 2, 20, 30, 45]
    //         myChart.data.labels = labels
    //         myChart.update();
    //     })
    
    //     $('#14days').on('click', function () {
    //         myChart.data.datasets[0].data = [100, 110, 105, 102, 120, 130, 145, 100, 110, 105, 102, 120, 130, 145]
    //         myChart.data.labels = labels2
    //         myChart.update();
    //     })
    // }

    // function initCaptureImagePage() {
    //     console.log('Capture image')
    
    
    //     const video = document.querySelector('#captureImagePage video')
    //     const canvas = document.querySelector('#captureImagePage canvas')
    //     const img = document.querySelector('#captureImagePage img')
    //     const ssBtn = document.querySelector('#captureImagePage #screenShot')
    //     const selectVideos = document.querySelector('#captureImagePage #videos')
    
    
    //     ssBtn.addEventListener('click', function () {
    //         canvas.width = video.videoWidth
    //         canvas.height = video.videoHeight
    
    //         canvas.getContext('2d').drawImage(video, 0, 0)
    //         const imageURL = canvas.toDataURL('image/webp')
    //         img.src = imageURL
    //     })
    
    
    //     function getStream() {
    
    //         const source = selectVideos.value
    
    //         const constrains = {
    //             video: {
    //                 deviceId: source ? { exact: source } : undefined
    //             }
    //         }
    
    //         return navigator.mediaDevices.getUserMedia(constrains).then(gotStream)
    //     }
    
    //     function gotStream(stream) {
    //         // console.log(stream.getVideoTracks()[0].id)
    //         // selectVideos.selectedIndex = [...selectVideos.options].findIndex(opt => opt)
    
    //         video.srcObject = stream
    //     }
    
    //     function getDevices() {
    //         return navigator.mediaDevices.enumerateDevices()
    //     }
    
    //     function gotDevices(deviceInfo) {
    
    //         const videoDevices = deviceInfo.filter(x => x.kind === 'videoinput')
    //         console.log(videoDevices)
    
    //         for (let i = 0; i < videoDevices.length; i++) {
    //             const device = videoDevices[i];
    
    //             const opt = document.createElement('option')
    //             opt.value = device.deviceId
    //             opt.text = `Camera ${i + 1} ${device.label || device.deviceId}`
    //             selectVideos.appendChild(opt)
    //         }
    
    //     }
    
    
    //     function stopStreamedVideo(e) {
    
    //         const stream = video.srcObject;
    //         const tracks = stream.getTracks();
    
    //         tracks.forEach(function (track) {
    //             track.stop();
    //         });
    
    //         video.srcObject = null;
    //     }
    
    //     document
    //         .querySelector('#stopStream')
    //         .addEventListener('click', stopStreamedVideo)
    
    //     getStream().then(getDevices).then(gotDevices)
    // }
    
    




    // Function for even and odd cards:
    // items.forEach(({ id }) => {
    //     if ($(id) % 2 === 0) {
    //         console.log('The card id is even')
    //         $(cardBody).addClass('even-card')
    //     } else {
    //         console.log('The card id is odd');
    //         $(cardBody).addClass('odd-card')
    //     }
    // })

    // for (let i = 0; i <= items.length; i++) {
    //     if (parseInt($(items.id)) % 2 === 0) {
    //         console.log('The card id is even')
    //         $(cardBody).addClass('even-card')
    //     } else {
    //         console.log('The card id is odd');
    //         $(cardBody).addClass('odd-card')
    //     }
    // }

    // $.get('https://jsonplaceholder.typicode.com/users').then(function(id){
    //     for (let i = 0; i <= items.length; i++) {
    //         if (parseInt($(id)) % 2 === 0) {
    //             console.log('The card id is even')
    //             $(cardBody).addClass('even-card')
    //         } else {
    //             console.log('The card id is odd');
    //             $(cardBody).addClass('odd-card')
    //         }
    //     }
    // })

    // Slide Images on Visitor Home Page:
    $('#exampleSlider').multislider();
    $('#exampleSlider').multislider('continuous')

})